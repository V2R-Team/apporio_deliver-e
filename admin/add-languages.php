<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
    $query="select * from languages";
    $result = $db->query($query);
    $list=$result->rows;
    

if(isset($_POST['save'])) {
    $language_name = $_POST['language_name'];
    $query="select * from languages Where language_name='$language_name'";
    $result = $db->query($query);
    $list=$result->rows;
    if(empty($list))
    {
		    $query = "INSERT INTO table_languages (language_name) VALUES ('$language_name')";
		    $db->query($query);
		     $msg = "Language Save Successfully";
                    echo '<script type="text/javascript">alert("'.$msg.'")</script>';
		    $db->redirect("home.php?pages=add-languages");
    }else{
              $msg = "This Language Is Already Added";
              echo '<script type="text/javascript">alert("'.$msg.'")</script>';
             $db->redirect("home.php?pages=add-languages"); 
    }
}
?>
  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add New Language For Application</h3>
        <span class="tp_rht">
            <!--<a href="home.php?pages=view-languages" class="btn btn-default btn-lg" id="add-button"  title="Back to Listing" role="button">Back to Listing</a>-->
            <a href="home.php?pages=view-languages" data-toggle="tooltip" title="Back" class="btn btn-default"><i class="fa fa-reply"></i></a>
      </span>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class=" form" >
              <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
              
                         <div class="form-group ">
                                <label class="control-label col-lg-2">Language</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="language_name" id="language_name">
                                        <option value="">--Please Select Language--</option>
                                        <?php foreach($list as $language){ ?>
                                            <option value="<?=  $language['name'];?>" ><?php echo $language['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            

                  <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
