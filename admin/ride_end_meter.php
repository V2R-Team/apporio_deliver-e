<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");

include 'pn_android.php';
include 'pn_iphone.php';

$ride_id = $_REQUEST['ride_id'];
$driver_id=$_REQUEST['driver_id'];
$end_lat=$_REQUEST['end_lat'];
$end_long=$_REQUEST['end_long'];
$end_location=$_REQUEST['end_location'];
$distance = $_REQUEST['distance'];
$driver_token=$_REQUEST['driver_token'];
$language_id=$_REQUEST['language_id'];

if($ride_id!="" && $driver_id!="" && $end_lat!="" && $end_long!="" && $end_location!="" && $driver_token!= "" )
{
    $query="select * from driver where driver_token='$driver_token' AND driver_id='$driver_id'";
    $result = $db->query($query);
    $ex_rows=$result->num_rows;
    $list = $result->row;
    if($ex_rows==1)
    {
        $last_time_stamp = date("h:i:s A");
        $end_time = date("h:i:s A");
        $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
        $data=$dt->format('M j');
        $day=date("l");
        $date=$day.", ".$data ;
        $new_time=date("h:i");
        $completed_rides = $list['completed_rides']+"1";
        $city_id = $list['city_id'];
        $car_type_id = $list['car_type_id'];
        $query5="UPDATE driver SET last_update='$new_time',completed_rides='$completed_rides' WHERE driver_id='$driver_id'" ;
        $db->query($query5);

        $query1="UPDATE ride_table SET driver_id='$driver_id' ,last_time_stamp='$last_time_stamp', ride_status='7' WHERE ride_id='$ride_id'" ;
        $db->query($query1);


        $query2 = "select * from price_card where city_id='$city_id' and car_type_id='$car_type_id'";
        $result2 = $db->query($query2);
        $list3 = $result2->row;
        $base_distance = $list3['base_distance'];
        $base_distance_price = $list3['base_distance_price'];
        $base_price_per_unit = $list3['base_price_per_unit'];

        $free_ride_minutes = $list3['free_ride_minutes'];
        $price_per_ride_minute = $list3['price_per_ride_minute'];

        $free_waiting_time = $list3['free_waiting_time'];
        $wating_price_minute = $list3['wating_price_minute'];

        $dist1 = ($distance/1000);
        if($dist1 <= $base_distance)
        {
            $final_amount = $base_distance_price;
        }
        else
        {
            $diff_distance = $dist1-$base_distance;
            $amount1= ($diff_distance * $base_price_per_unit);
            $final_amount = $base_distance_price+$amount1;
        }
        $query3="select * from done_ride where ride_id='$ride_id'";
        $result3 = $db->query($query3);
        $list=$result3->row;
        $done_ride=$list['done_ride_id'];
        $begin_time = $list['begin_time'];
        $waiting_time = $list['waiting_time'];


        $datetime1 = strtotime($begin_time);
        $datetime2 = strtotime($end_time);
        $interval  = abs($datetime2 - $datetime1);
        $ride_time   = round($interval / 60);
        

        $query2="UPDATE done_ride SET end_lat='$end_lat',end_long='$end_long', end_location='$end_location', end_time='$end_time', amount='$final_amount', distance='$dist1',tot_time='$ride_time' WHERE ride_id='$ride_id'";
        $db->query($query2);


        if($ride_time > $free_ride_minutes)
        {
                  $diff_distance = $ride_time-$free_ride_minutes;
                 $amount1= ($diff_distance * $price_per_ride_minute);
            $query2="UPDATE done_ride SET  ride_time_price='$amount1' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }else{
            $query2="UPDATE done_ride SET  ride_time_price=0 WHERE ride_id='$ride_id'";
            $db->query($query2);
        }

       
        if($waiting_time > $free_waiting_time)
        {
            $diff_distance = $waiting_time-$free_waiting_time;
            $amount1= ($diff_distance * $wating_price_minute);
            $query2="UPDATE done_ride SET waiting_price='$amount1' WHERE ride_id='$ride_id'";
            $db->query($query2);
        }else{
            $query2="UPDATE done_ride SET waiting_price=0 WHERE ride_id='$ride_id'";
            $db->query($query2);
        }

         $query4="select * from ride_table where ride_id='$ride_id'";
        $result4 = $db->query($query4);
        $list4=$result4->row;
        $user_id=$list4['user_id'];
        $ride_status = $list4['ride_status'];
        $payment_option_id=$list4['payment_option_id'];

        $list['payment_option_id']=$payment_option_id;


        $query5="select * from user where user_id='$user_id'";
        $result5 = $db->query($query5);
        $list5=$result5->row;
        $device_id=$list5['device_id'];
        $language="select * from messages where language_id='$language_id' and message_id=28";
        $lang_result = $db->query($language);
        $lang_list=$lang_result->row;

        $message=$lang_list['message_name'];

        $ride_id= (String) $done_ride;
        $ride_status= (String) $ride_status;

        if($device_id!="")
        {
            if($list5['flag'] == 1)
            {
                IphonePushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
            }
            else
            {
                AndroidPushNotificationCustomer($device_id, $message,$ride_id,$ride_status);
            }
        }

        $re = array('result'=> 1,'msg'=> $message,'details'	=> $list);

    }
    else {
        $re = array('result'=> 419,'msg'=> "No Record Found",);
    }
}
else
{
    $re = array('result'=> 0,'msg'=> "Required fields missing!!",);
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>