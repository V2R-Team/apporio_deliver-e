<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
$query="select * from role ORDER BY role_id";
	$result = $db->query($query);
	$list=$result->rows;

    if(isset($_GET['status']) && isset($_GET['id'])) 
    {
     $query1="UPDATE role SET role_status='".$_GET['status']."' WHERE role_id='".$_GET['id']."'";
     $db->query($query1);
     $db->redirect("home.php?pages=view-role");
    }

    if(isset($_POST['command']) && $_POST['command'] == "m delete") {
    $ids = implode(",",$_POST['chk']);
    $query1="DELETE FROM role WHERE role_id IN ($ids) ";
      $db->query($query1);
      $db->redirect("home.php?pages=view-role");
  }
	
    
?>

<div class="wraper container-fluid">
  <div class="page-title">
    <h3 class="title">View Role</h3>   
 </div>
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
              <table id="datatable" class="table table-striped table-bordered table-responsive">
                <thead>
                  <tr>
                    <th width="5%">S.No</th>
                    <th>Role Name</th>
                    <th width="8%">Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($list as $city){?>
                  <tr>
                    <td><?php echo $city['role_id'];?></td>
                     
                    <td>
                      
                  				<a title="Edit" href="home.php?pages=add-role&edit_id=<?=$city['role_id']?>" style="text-transform: capitalize;"><?=$city['role_name']?></a>
                    </td>
                    <?php
                                if($city['role_status']==1) {
                                ?>
                                <td class="text-center">
                                    <a href="home.php?pages=view-role&status=2&id=<?php echo $city['role_id']?>" class="" title="Active">
                                    <button type="button" class="btn btn-success br2 btn-xs fs12 activebtn" > Active
                                    </button></a>
                                </td>
                                <?php
                                } else {
                                ?>
                                <td class="text-center">
                                <a href="home.php?pages=view-role&status=1&id=<?php echo $city['role_id']?>" class="" title="Deactive">
                                    <button type="button" class="btn btn-danger  br2 btn-xs fs12 dropdown-toggle" > Deactive
                                    </button></a>
                                </td>
                            <?php } ?>
                  </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End row --> 
  
</div>
</form>


<!-- Page Content Ends --> 
<!-- ================== -->

</section>
<!-- Main Content Ends -->

</body></html>