-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:08 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_deliver-e`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'apporio', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(20, 'demo', 'demo', 'demo', '', '', '123456', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(21, 'demo1', 'demo1', 'demo1', '', '', '1234567', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '3', 0, 1),
(22, 'aamir', 'Brar Sahab', 'appppp', '', '', '1234567', '98238923929', 'hello@info.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 2),
(23, 'Rene ', 'Ortega Villanueva', 'reneortega', '', '', 'ortega123456', '990994778', 'rene_03_10@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(24, 'Ali', 'Alkarori', 'ali', '', '', 'Isudana', '4803221216', 'alikarori@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(25, 'Ali', 'Karori', 'aliKarori', '', '', 'apporio7788', '480-322-1216', 'sudanzol@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(26, 'Shilpa', 'Goyal', 'shilpa', '', '', '123456', '8130039030', 'shilpa@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1),
(27, 'siavash', 'rezayi', 'siavash', '', '', '123456', '+989123445028', 'siavash55r@yahoo.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(28, 'Murt', 'Omer', 'Murtada', '', '', '78787878', '2499676767676', 'murtada@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(29, 'tito', 'reyes', 'tito', '', '', 'tito123', '9999999999', 'tito@reyes.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(30, 'ANDRE ', 'FREITAS', 'MOTOTAXISP', '', '', '14127603', '5511958557088', 'ANDREFREITASALVES2017@GMAIL.COM', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(31, 'exeerc', 'exeerv', 'exeerc', '', '', 'exeeerv', '011111111111', 'exeer@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(32, 'rsdcdulax', 'rsdcdulax', 'rsdcdulax', '', '', 'rsdcdulax', '+56982428020', 'rsdcdulax@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(33, 'jesus ', 'pajares', 'jph', '', '', 'soloyolose', '992233551', 'jesuspajareshorna@gmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(34, 'alvin', 'wong', 'alvin123', '', '', '123456', '123123123123', 'weelin93@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1),
(35, 'alvin', 'wong', 'alvindispatcher', '', '', '123456', '1293012937', 'weelin93@hotmail.com', '', '', '', '', '', 0, '', '', '', '', '', '', '2', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`) VALUES
(1, 'Deliver-e', 'uploads/logo/logo_59eed066e94d4.png', 'hello@apporio.com', 'Gurugram, Haryana, India', 'AIzaSyApdyEoXJINkTeCUs6Dpz47Ma33-aNCBiQ', '28.4594965', '77.0266383', 'calltaxee');

-- --------------------------------------------------------

--
-- Table structure for table `All_Currencies`
--

CREATE TABLE `All_Currencies` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `All_Currencies`
--

INSERT INTO `All_Currencies` (`id`, `currency_name`) VALUES
(1, 'ARIARY'),
(2, 'AUSTRAL'),
(3, 'BHAT'),
(4, 'BIRR'),
(5, 'BOLIVAR'),
(6, 'BOLIVIANO'),
(7, 'CEDI'),
(8, 'CENT'),
(9, 'COLON'),
(10, 'CORDOBA'),
(11, 'CRUZEIRO'),
(12, 'DALASI'),
(13, 'DINAR'),
(14, 'DIRHAM'),
(15, 'DOBRA'),
(16, 'DOLLAR'),
(17, 'DONG'),
(18, 'DRACHMA'),
(19, 'DRAM'),
(20, 'ESCUDO'),
(21, 'EURO'),
(22, 'FLORIN'),
(23, 'FORINT'),
(24, 'FRANC'),
(25, 'GOURDE'),
(26, 'GUARANI'),
(27, 'GUILDER'),
(28, 'HRYVNIA'),
(29, 'KINA'),
(30, 'KIP'),
(31, 'KORUNA'),
(32, 'KRONE'),
(33, 'KUNA'),
(34, 'KWACHA'),
(35, 'KWANZA'),
(36, 'KYAT'),
(37, 'LARI'),
(38, 'LEK'),
(39, 'LEMPIRA'),
(40, 'LEONE'),
(41, 'LEU'),
(42, 'LEV'),
(43, 'LILANGENI'),
(44, 'LIRA'),
(45, 'LIVRE TOURNOIS'),
(46, 'LOTI'),
(47, 'MANTA'),
(48, 'METICAL'),
(49, 'MILL'),
(50, 'NAIRA'),
(51, 'NAKFA'),
(52, 'NGULTRM'),
(53, 'OUGUIYA'),
(54, 'PAANGA'),
(55, 'PATACA'),
(56, 'PENNY'),
(57, 'PESETA'),
(58, 'PESO'),
(59, 'POUND'),
(60, 'PULA'),
(61, 'QUETZAL'),
(62, 'RAND'),
(63, 'REAL'),
(64, 'RIAL'),
(65, 'RIEL'),
(66, 'RINGGIT'),
(67, 'RIYAL'),
(68, 'RUBEL'),
(69, 'RUFIYAA'),
(70, 'RUPEE'),
(71, 'RUPIAH'),
(72, 'SHEQEL'),
(73, 'SHILING'),
(74, 'SOL'),
(75, 'SOM'),
(76, 'SOMONI'),
(77, 'SPESMILO'),
(78, 'STERLING'),
(79, 'TAKA'),
(80, 'TALA'),
(81, 'TENGE'),
(82, 'TUGRIK'),
(83, 'VATU'),
(84, 'WON'),
(85, 'YEN'),
(86, 'YUAN'),
(87, 'ZLOTY');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refused to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(13, 'i got lift', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer not arrived', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 2),
(2, 'Suzuki', 'uploads/car/car_2.png', 2),
(3, 'Ferrari', 'uploads/car/car_3.png', 2),
(4, 'Lamborghini', 'uploads/car/car_4.png', 2),
(5, 'Mercedes', 'uploads/car/car_5.png', 2),
(6, 'Tesla', 'uploads/car/car_6.png', 2),
(10, 'Renault', 'uploads/car/car_10.jpg', 2),
(11, 'taxi', 'uploads/car/car_11.png', 2),
(12, 'taxi', 'uploads/car/car_12.jpg', 2),
(13, 'yamaha', 'uploads/car/car_13.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(2, 'Creta', '', '', '', '', '', '', '', 3, 1),
(3, 'Nano', '', '', '', '', '', '', '', 2, 1),
(6, 'Audi Q7', '', '', '', '', '', '', '', 1, 1),
(8, 'Alto', '', '', '', '', '', '', '', 8, 1),
(11, 'Audi Q7', '', '', '', '', '', '', '', 4, 1),
(20, 'Sunny', '', 'Sunny', 'Nissan', '2016', '2017', 'White', 'uploads/car/editcar_20.png', 3, 1),
(16, 'Korola', '', '', '', '', '', '', '', 25, 1),
(17, 'BMW 350', '', '', '', '', '', '', '', 26, 1),
(18, 'TATA', '', 'TATA', '', '', '', '', '', 5, 1),
(19, 'Eco Sport', '', '', '', '', '', '', '', 28, 1),
(29, 'MUSTANG', '', 'MUSTANG', 'FORD', '2016', '2017', 'Red', '', 4, 1),
(31, 'Nano', '', 'Nano', 'TATA', '2017', '2017', 'Yellow', '', 3, 1),
(40, 'door to door', '', '', 'taxi', '', '', '', '', 12, 1),
(41, 'var', '', '', 'taxi', '', '', '', '', 13, 1),
(42, 'van', '', '', 'Suzuki', '', '', '', '', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `car_type_name`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `ride_mode`, `car_admin_status`) VALUES
(2, 'HATCHBACK', '', 'HATCHBACK', 'uploads/car/editcar_2.png', 'webstatic/img/fleet-image/prime-play.png', 1, 1),
(3, 'LUXURY', '', 'LUXURY', 'uploads/car/editcar_3.png', 'webstatic/img/fleet-image/lux.png', 1, 1),
(4, 'Mini', '', 'Mini', 'uploads/car/editcar_4.png', 'webstatic/img/fleet-image/mini.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(56, 'Gurugram', '', '', '&#x20a8', 'AUD', '0024', 'Miles', '', '', 1),
(122, 'Pleasanton', '', '', '&#x24;', 'AUD', '0024', 'Km', '', '', 1),
(123, 'Columbia', '34.0007104', '-81.0348144', '&#x24;', 'AUD', '0024', 'Miles', '', '', 1),
(124, 'MedellÃ­n - Antioquia', '6.244203', '-75.5812119', '&#x24;', 'AUD', '0024', 'Miles', '', '', 1),
(125, 'Envigado - Antioquia', '', '', '&#x24;', 'AUD', '0024', 'Miles', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `configuration_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `rider_phone_verification` int(11) NOT NULL,
  `rider_email_verification` int(11) NOT NULL,
  `driver_phone_verification` int(11) NOT NULL,
  `driver_email_verification` int(11) NOT NULL,
  `email_name` varchar(255) NOT NULL,
  `email_header_name` varchar(255) NOT NULL,
  `email_footer_name` varchar(255) NOT NULL,
  `reply_email` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_footer` varchar(255) NOT NULL,
  `support_number` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_address` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`configuration_id`, `country_id`, `project_name`, `rider_phone_verification`, `rider_email_verification`, `driver_phone_verification`, `driver_email_verification`, `email_name`, `email_header_name`, `email_footer_name`, `reply_email`, `admin_email`, `admin_footer`, `support_number`, `company_name`, `company_address`) VALUES
(1, 30, 'k10', 1, 2, 1, 2, 'k10', 'Apporio Infolabs', 'k10', 'apporio@info.com2', '', 'k10', '', 'k10', 'algeria');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `skypho`, `subject`) VALUES
(18, 'lkdsasalsa', 'msammsa@qkjsa', 'kddepoew320-', 'saksalask'),
(17, 'qajkalk', 'SAJD@JSAG', '12344555666', 'kaal;al'),
(16, 'Yogesh', 'yogeshkumar2491@gmail.com', '', 'Testing'),
(15, 'aaritnlh', 'sample@email.tst', '555-666-0606', '1'),
(14, 'ZAP', 'foo-bar@example.com', 'ZAP', 'ZAP'),
(13, 'Hani', 'ebedhani@gmail.com', '05338535001', 'Your app is good but had some bugs, sometimes get crash !!'),
(19, 'anurag', 'yuio@wkjlkew', '328740932', 'lfejljfsl'),
(20, 'sdllksd', 'skjs@lkdslk', 'lklkds', 'dlkslk'),
(21, 'dffhh', 'fhhjk@ghgj', '986987587', 'gkkhll'),
(22, 'asqwq', 'qaz@gmail.com', '2321235435', 'ertewtterterttrtrrttr');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '+91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`coupons_id`, `coupons_code`, `coupons_price`, `total_usage_limit`, `per_user_limit`, `start_date`, `expiry_date`, `coupon_type`, `status`) VALUES
(73, 'APPORIOINFOLABS', '10', 0, 0, '2017-06-14', '2017-06-15', 'Nominal', 1),
(75, 'JUNE', '10', 0, 0, '2017-06-16', '2017-06-17', 'Percentage', 1),
(76, 'ios', '20', 0, 0, '2017-06-28', '2017-06-29', 'Nominal', 1),
(78, 'murtada', '10', 0, 0, '2017-07-09', '2017-07-12', 'Percentage', 1),
(80, 'aaabb', '10', 0, 0, '2017-07-13', '2017-07-13', 'Nominal', 1),
(81, 'TODAYS', '10', 0, 0, '2017-07-18', '2017-07-27', 'Nominal', 1),
(82, 'CODE', '10', 0, 0, '2017-07-19', '2017-07-28', 'Percentage', 1),
(83, 'CODE1', '10', 0, 0, '2017-07-19', '2017-07-27', 'Percentage', 1),
(84, 'GST', '20', 0, 0, '2017-07-19', '2017-07-31', 'Nominal', 1),
(85, 'PVR', '10', 0, 0, '2017-07-21', '2017-07-31', 'Nominal', 1),
(86, 'PROMO', '5', 0, 0, '2017-07-20', '2017-07-29', 'Percentage', 1),
(87, 'TODAY1', '10', 0, 0, '2017-07-25', '2017-07-28', 'Percentage', 1),
(88, 'PC', '10', 0, 0, '2017-07-27', '2017-07-31', 'Nominal', 1),
(89, '123456', '100', 0, 0, '2017-07-29', '2018-06-30', 'Nominal', 1),
(90, 'PRMM', '20', 0, 0, '2017-08-03', '2017-08-31', 'Nominal', 1),
(91, 'NEW', '10', 0, 0, '2017-08-05', '2017-08-09', 'Percentage', 1),
(92, 'HAPPY', '10', 0, 0, '2017-08-10', '2017-08-12', 'Nominal', 1),
(93, 'EDULHAJJ', '2', 0, 0, '2017-08-31', '2017-09-30', 'Nominal', 1),
(94, 'SAMIR', '332', 0, 0, '2017-08-17', '2017-08-30', 'Nominal', 1),
(95, 'APPLAUNCH', '200', 0, 0, '2017-08-23', '2017-08-31', 'Nominal', 1),
(96, 'SHILPA', '250', 5, 2, '2017-08-26', '2017-08-31', 'Nominal', 1),
(97, 'ABCD', '10', 1, 1, '2017-08-26', '2017-08-31', 'Nominal', 1),
(98, 'alak', '10', 200, 1, '2017-08-27', '2017-08-31', 'Percentage', 1),
(99, 'mahdimahdi', '15', 0, 0, '2017-08-29', '2017-08-31', 'Percentage', 1),
(100, 'mahdimahdi', '15', 1000000000, 100000, '2017-08-29', '2017-08-31', 'Percentage', 1),
(101, 'JULIO', '2', 100, 1, '2017-09-01', '2017-09-07', 'Nominal', 1),
(102, 'APPORIO', '10', 10, 10, '2017-09-02', '2017-09-30', 'Nominal', 1),
(103, 'OLA', '10', 10, 10, '2017-09-02', '2017-09-14', 'Nominal', 1),
(104, 'september', '100', 1, 1, '2017-09-04', '2017-09-30', 'Nominal', 1),
(105, 'Namit', '20', 100, 100, '2017-09-05', '2017-09-15', 'Nominal', 1),
(106, 'Samir', '12', 23, 2, '2017-09-11', '2017-09-29', 'Nominal', 1),
(107, '1000', '50', 100, 100, '2017-09-12', '2017-09-23', 'Nominal', 1),
(108, 'Manual', '10', 10, 10, '2017-09-19', '2017-09-22', 'Nominal', 1),
(109, '213131', '10', 200, 0, 'NaN-NaN-NaN', 'NaN-NaN-NaN', 'Nominal', 1),
(110, 'p898', '100', 1, 1, '2017-09-25', '2017-09-25', 'Percentage', 1),
(111, '1514', '16', 0, 0, 'NaN-NaN-NaN', 'NaN-NaN-NaN', 'Percentage', 1),
(112, '20', '20', 1234336, 7667666, '2017-09-30', '2017-10-31', 'Nominal', 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(100) DEFAULT NULL,
  `currency_html_code` varchar(100) DEFAULT NULL,
  `currency_unicode` varchar(100) DEFAULT NULL,
  `currency_isocode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_html_code`, `currency_unicode`, `currency_isocode`) VALUES
(6, 16, '&#x24;', '00024', 'USD ');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_support`
--

INSERT INTO `customer_support` (`customer_support_id`, `application`, `name`, `email`, `phone`, `query`, `date`) VALUES
(1, 1, 'ananna', 'anaknak', '939293', 'kaksak', 'Saturday, Aug 26, 01:05 PM'),
(2, 2, 'xhxhxhhx', 'jcjcjcjcjc45@t.com', '1234567990', 'xhlduohldgkzDJtisxkggzk', 'Saturday, Aug 26, 01:06 PM'),
(3, 2, 'dyhddh', 'hxhxjccj57@y.com', '3664674456', 'chicxgxhxyhccphxoh', 'Saturday, Aug 26, 01:10 PM'),
(4, 2, 'bzxhjx', 'xhcjlbgkog46@t.com', '488668966', 'hxcjycpots gkkcDuizgk g kbclh', 'Saturday, Aug 26, 01:12 PM'),
(5, 1, 'hdxhxh', 'pcijcjcjc56@y.com', '63589669', 'bhxcjn tfg nzgogxoxgxl', 'Saturday, Aug 26, 01:46 PM'),
(6, 2, 'fbfbfbfb', 'fbfbfbfb', 'hdhdhhdhdhdh', 'udydhdhdg', 'Saturday, Aug 26, 06:20 PM'),
(7, 2, 'd xrcr', 'd xrcr', 'wzxxwxwxe', 'ssxscecececrcrcrcrcrcrcrc', 'Saturday, Aug 26, 06:25 PM'),
(8, 2, 'Shilpa', 'Shilpa', '9865321478', 'hi ', 'Sunday, Aug 27, 09:25 AM'),
(9, 2, 'hsjhhshh', 'hsjhhshh', '9969569', 'vvvvvvgg', 'Monday, Aug 28, 11:17 AM'),
(10, 1, 'Shivani', 'fxuxyuguhco@gmail.com', 'trafsyxtuxfuxfu', 'Fyfzyhf hf', 'Monday, Aug 28, 11:25 AM'),
(11, 2, 'zfjzgjzjgzgj', 'jfititig@gmail.com', 'dghvvch', 'Dfhxxhhxhc', 'Monday, Aug 28, 11:26 AM'),
(12, 2, 'dhjsjz', 'dhjsjz', '76979797979', 'zhhzhzhzhhz', 'Monday, Aug 28, 05:28 PM'),
(13, 1, 'Halmat ', 'LuxuryRidesLimo@gmail.com', '8477645466', 'Hey this is Halmat \nCan you call me or email me \nNeed to talk to you\n\nThanks ', 'Wednesday, Sep 6, 08:38 PM'),
(14, 2, 'anuuuuuu', 'anuuuuuu', '8874531856', 'jftjffugjvvd', 'Thursday, Sep 7, 10:45 AM'),
(15, 2, 'André', 'André', '1195855708870', 'seja bem-vindo ', 'Tuesday, Sep 12, 05:38 AM'),
(16, 2, 'anurag', 'anurag', '880858557585', 'zfgfgfgxxfdcghdgc', 'Tuesday, Sep 12, 11:20 AM'),
(17, 2, 'MOHAMED ', 'MOHAMED ', '8639206529', 'taxi app', 'Wednesday, Sep 13, 11:17 PM'),
(18, 2, 'amrit', 'amrit', '0686566855', 'ucjcjyjbkvffvkb', 'Thursday, Sep 14, 10:56 AM'),
(19, 2, 'ndufu', 'ndufu', '27342438', 'ududcucuc', 'Thursday, Sep 14, 11:06 AM'),
(20, 2, '15666666666', '15666666666', '15666666666', '15666666666', 'Friday, Sep 15, 05:55 AM'),
(21, 2, '15666666666', '15666666666', '15666666666', '15666666666', 'Saturday, Sep 16, 12:08 AM'),
(22, 2, 'Adamah', 'adamah.welbeck@gmail.com', '0208219759', 'My system keep going off. Please try to help me', 'Wednesday, Sep 20, 12:29 AM'),
(23, 1, 'Adamah Welbeck ', 'info@me.com', '0208219759', 'Hello how you doing I have problem with system', 'Wednesday, Sep 20, 10:18 PM'),
(24, 2, '15666666666', '15666666666', '15666666666', '15666666666', 'Thursday, Sep 21, 06:29 PM'),
(25, 2, 'Yogesh', 'Yadav@gmail.com', '12345', 'Query\r\n', 'Thursday,Sep 28,05:42 AM'),
(26, 2, 'Yogesh Testing', 'tests@f.com', '123456', 'helllo', 'Thursday,Sep 28,06:09 AM'),
(27, 2, 'Yogesh Testing', 'tests@f.com', '123456', 'helllo', 'Thursday,Sep 28,06:10 AM'),
(28, 2, 'test', 'test@gmil.com', '12345', '12121', 'Thursday,Sep 28,06:10 AM'),
(29, 2, 'asqwq', 'qaz@gmail.com', '2321235435', 'ertewtterterttrtrrttr', 'Thursday,Sep 28,06:15 AM'),
(30, 2, 'anurag', 'anurag@gmail.com', '98987686987697', 'akhkhjlalajjl', 'Thursday,Sep 28,06:15 AM'),
(31, 2, 'asd', '1wwsd@ddd', 'dsd', 'ddddddddddd', 'Thursday,Sep 28,06:16 AM'),
(32, 2, 'ewqla', 'hjkjklkl@nm.mlk', '6998009878', 'hlj;lk;ljhgfg', 'Thursday,Sep 28,01:08 PM'),
(33, 2, 'Testing ', 'Testing ', '080885455458', 'Testing ', 'Saturday, Sep 30, 04:45 AM'),
(34, 2, 'aa', 'ad@wsfd', '242323132', 'dsaffffsafasf', 'Tuesday,Oct 3,05:45 AM'),
(35, 2, 'dsaff', 'asf@wsaf', '12982170', 'salkjdsahkjgfy', 'Tuesday,Oct 3,05:45 AM'),
(36, 2, 'sayuyuau', 'hjgjhjakahjy@hjwsa', 'xhjahjsa', 'skakhlk', 'Tuesday,Oct 3,05:46 AM'),
(37, 2, 'Pedro', 'pro@data.xom', 'highs', 'Pro and con error', 'Sunday, Nov 5, 07:16 AM');

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.4120953022849', '77.0432441750765', '28.4122253897939', '77.0435084002466', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '06:51:04 AM', '06:51:16 AM', '06:51:32 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00'),
(2, 4, '28.4121408785035', '77.043345652569', '28.4592693', '77.0724192', '68, Plaza St, Block S, Uppal Southend, Sector 49, Gurugram, Haryana 122018, India', 'Netaji Subhash Marg, Sector 44, Gurugram, Haryana 122003, India', '11:51:53 AM', '11:52:19 AM', '11:52:29 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 1, '100', '4.00', '96.00', '100.00', '0.00', '0.00 Miles', '0.0', '0', '100', 1, '', 1, 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(1, 0, 4, 'test', 'test@gmail.com', '9205361756', '', 'qwerty', 'b6DlgDwlnIElhdSj', '192', '8', '', 'D800399967D467778B3B1A31AAC4F23B89DD44A5F611E57E017C9302FB632C93', 1, '4', 2, 3, '2345', 56, 'Friday, Oct 6', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122152313193', '77.0434326005507', '68, Plaza Street, Block S, Uppal Southend, Sector 49 , Gurugram, Haryana 122018', '06:29', 'Tuesday, Oct 24, 2017', 2, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-06', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(2, 0, 50, 'Delivertest', 'delivere@gmail.com', '9215851756', 'uploads/driver/1508811336driver_2.jpg', 'qwerty', 'Fu7hBTGKSzlZ9IAf', '0', '0', '', '96F2DBF17332AF6D3452040CF67CDC000B38FFFBA7677143046ECF8240BBA9F1', 1, '', 2, 3, '6545', 123, 'Friday, Oct 6', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.17442214016243', '-75.5908774566205', 'Carrera 47 #35a Sur-1 a 35a Sur-73 , Envigado, Antioquia', '12:52', 'Tuesday, Oct 24, 2017', 0, 0, 0, 2, 0, 2, 2, 0, '', 1, '', '2017-10-06', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(3, 0, 10, 'new test ', 'newnew@gmail.com', '9205561756', '', 'qwerty', 'ZLlDHM1mqtJE2814', '0', '0', '', '96F2DBF17332AF6D3452040CF67CDC000B38FFFBA7677143046ECF8240BBA9F1', 1, '', 2, 3, '5566', 124, 'Tuesday, Oct 24', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.17427973641936', '-75.590648417418', 'Carrera 47 #35 Sur-2 a 35 Sur-74 , Envigado, Antioquia', '13:24', 'Tuesday, Oct 24, 2017', 0, 0, 0, 1, 0, 2, 2, 0, '', 1, '', '2017-10-24', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0),
(4, 0, 10, 'Juan Sebastian Gil ', 'jsgil@kasadigital.com', '3137072180', '', 'zxcasdqwe123', '691JTGoJbdIEHdP9', '0', '0', '', 'C057C98E4D1E451D6647C61C9F628C597046B3F3F43A0B24655BDED53BC4B2B4', 1, '', 2, 3, 'hkh78g', 124, 'Sunday, Nov 5', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '6.17424405606739', '-75.5906126021945', 'Carrera 46a #35 Sur-1 a 35 Sur-75 , Envigado, Antioquia', '21:03', 'Monday, Nov 6, 2017', 0, 0, 0, 1, 0, 2, 2, 0, '', 1, '', '2017-11-05', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `date`) VALUES
(1, 1, '200', 2, '192', '8', '2017-10-06');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 1, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_name`, `path`) VALUES
(1, 'hello', 'http://www.apporiotaxi.com/Apporiotaxi/test_docs/Capture1.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', '<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" 22px=\"\" !important;=\"\" 0)=\"\" 0,=\"\" rgb(0,=\"\" 0px;=\"\" 1.5;=\"\" sans\";=\"\" open=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" 0px;=\"\" sans\";=\"\" open=\"\" initial;\"=\"\" initial;=\"\" 255);=\"\" 255,=\"\" rgb(255,=\"\" none;=\"\" start;=\"\" 2;=\"\" normal;=\"\" 14px;=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" !important;\"=\"\" !important;=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\" normal;=\"\" sans\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" sans\";=\"\" open=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img width=\"232\" height=\"163\" class=\"alignnone size-full wp-image-434\" alt=\"iso_apporio\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" !important;\"=\"\" 5px=\"\" 0px=\"\" 10px=\"\" !important;=\"\" 0px;=\"\" sans\";=\"\" open=\"\" 17px=\"\" 51);=\"\" 51,=\"\" rgb(51,=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" !important;\"=\"\" sans\";=\"\" open=\"\" 102)=\"\" 102,=\"\" rgb(102,=\"\" 1.7;=\"\" justify;=\"\" 13px;=\"\"><br></p>\r\n\r\n</h2>\r\n\r\n<a> </a>', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '100', 'Friday, Oct 6', '1'),
(2, 2, 1, '1', 'Cash', 'Admin', '100', 'Friday, Oct 6', '1');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(3, 56, 'Miles', '', 1, 5, 0, 0, 0, 0, 0, 0, '3', '50', '25', '3', '10', '2', '15'),
(12, 56, 'Miles', '', 2, 4, 0, 0, 0, 0, 0, 0, '4', '100', '17', '1', '10', '1', '15'),
(13, 56, 'Miles', '', 3, 5, 0, 0, 0, 0, 0, 0, '4', '100', '16', '1', '10', '1', '15'),
(14, 56, 'Miles', '', 4, 6, 0, 0, 0, 0, 0, 0, '4', '178', '38', '3', '10', '2', '15'),
(15, 56, 'Miles', '', 5, 7, 0, 0, 0, 0, 0, 0, '4', '100', '20', '1', '10', '1', '12'),
(47, 123, 'Miles', '6', 2, 50, 0, 0, 0, 0, 0, 0, '10', '100', '10', '5', '10', '20', '20'),
(48, 123, 'Miles', '6', 3, 30, 0, 0, 0, 0, 0, 0, '10', '200', '20', '5', '10', '5', '10'),
(49, 124, 'Miles', '6', 2, 10, 0, 0, 0, 0, 0, 0, '100', '10', '10', '5', '5', '2', '12'),
(50, 124, 'Miles', '6', 3, 10, 0, 0, 0, 0, 0, 0, '200', '20', '20', '10', '10', '10', '5'),
(51, 125, 'Miles', '6', 4, 10, 0, 0, 0, 0, 0, 0, '100', '10', '10', '5', '5', '2', '2'),
(52, 125, 'Miles', '6', 3, 10, 0, 0, 0, 0, 0, 0, '100', '150', '10', '1010', '5', '5', '5');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'Hi Testing', 'Hi this is a testing message.', 'uploads/notification/1500380956793.jpg', 'http://carvicabs.com/', 576, 0, '2017-09-08', 1),
(2, 'App', 'tyuiighhbnbnm', 'uploads/notification/1500380956793.jpg', 'go.sd', 0, 250, '2017-09-08', 2),
(3, 'careeeeeem', 'yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 'uploads/notification/1500380956793.jpg', 'facebook.com', 0, 250, '2017-09-08', 2),
(4, 'Tedt', 'Test', 'uploads/notification/1500380956793.jpg', 'http://carvicabs.com', 576, 0, '2017-09-08', 1),
(5, 'Use Coupon NCR99', 'Get Rs 50 off on your first ride', 'uploads/notification/push_image_5.jpg', 'WWW.APPORIO.COM', 0, 0, '2017-09-11', 1),
(6, 'Hello Arvind', 'Hello arvind testing message', 'uploads/notification/1500380956793.jpg', 'http://carvicabs.com/', 576, 0, '2017-09-11', 1),
(7, 'Title of message', 'Body of message', 'uploads/notification/1500380956793.jpg', 'http://www.apporiotaxi.com', 601, 0, '2017-09-11', 1),
(8, 'Title of message', 'Body of message', 'uploads/notification/1500380956793.jpg', 'http://www.apporiotaxi.com', 581, 0, '2017-09-11', 1),
(9, '50% off', 'Apply Coupon TODAY ', 'uploads/notification/push_image_9.jpg', 'WWW.APPORIO.COM', 0, 0, '2017-09-12', 2),
(10, 'Hi Testing', 'asdj dsajj jfkla', 'uploads/notification/1500380956793.jpg', 'http://carvicabs.com/', 576, 0, '2017-09-12', 1),
(11, 'test', 'test', 'uploads/notification/1500380956793.jpg', '123', 0, 0, '2017-09-12', 1),
(12, 'Hello Arvind', 'ksdjal jsakdjfl; asdkl;f sjdkl;fa', 'uploads/notification/1500380956793.jpg', 'http://carvicabs.com/', 0, 462, '2017-09-12', 2),
(13, 'Hello Arvind', 'sdaf fs dfasdf sdaf ', 'uploads/notification/push_image_13.jpg', 'http://google.com/', 0, 0, '2017-09-12', 1),
(14, 'sdadadads', 'sasas', 'uploads/notification/1500380956793.jpg', 'sa', 318, 0, '2017-09-16', 1),
(15, 'Hello Arvind Test', 'Test Test', 'uploads/notification/1500380956793.jpg', 'http://google.com/', 576, 0, '2017-09-19', 1),
(16, 'gyghjgh', 'hgyuguyg', 'uploads/notification/1500380956793.jpg', '', 0, 662, '2017-09-29', 2),
(17, 'hkkhhkj', 'gyygjgyh', 'uploads/notification/1500380956793.jpg', '', 0, 662, '2017-09-29', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 1, 1, '2017-10-06'),
(2, 2, 1, 0, '2017-10-06'),
(3, 3, 1, 0, '2017-10-06'),
(4, 4, 1, 1, '2017-10-06');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.4121859831334', '77.043486339191', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '06:50:31', '06:51:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121859831334,77.043486339191&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06'),
(2, 1, '', '28.4121165780428', '77.0433073885698', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '07:11:14', '07:11:14 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121165780428,77.0433073885698&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-10-06'),
(3, 1, '', '28.4121165780428', '77.0433073885698', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '07:12:36', '07:12:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121165780428,77.0433073885698&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 2, 1, 1, 2, 0, 0, 4, 0, 1, 1, '2017-10-06'),
(4, 1, '', '28.4121408785035', '77.043345652569', '68, Plaza Street, Block S, Uppal Southend, Sector 49\nGurugram, Haryana 122018', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Oct 6', '11:51:15', '11:52:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.4121408785035,77.043345652569&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 1, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-06');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 2),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1),
(6, 'Breakdown', '199', 1),
(7, 'Breakdown', '199', 1),
(8, 'K10', '0800187241', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO'),
(6, 643, 'ewqla', 'lsajlas@jdsal', 'lsalk', 'lsalk');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive'),
(10, 'Ø§Ù„Ø±Ø®ØµØ©'),
(11, 'Ø§Ù„Ø¨Ø·Ø§Ù‚Ø©'),
(12, 'ØµÙˆØ±Ø©');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(24, 123, 3, 1),
(25, 124, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(1, 1, 3, 'uploads/driver/1507266746document_image_13.jpg', '2017-12-01', 1),
(2, 1, 2, 'uploads/driver/1507266757document_image_12.jpg', '2017-12-22', 1),
(3, 1, 4, 'uploads/driver/1507266770document_image_14.jpg', '2017-11-30', 1),
(4, 2, 3, 'uploads/driver/1507293458document_image_23.jpg', '2017-11-30', 1),
(5, 3, 3, 'uploads/driver/1508846139document_image_33.jpg', '2017-12-28', 1),
(6, 4, 3, 'uploads/driver/1509845689document_image_43.jpg', '2017-11-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 1, '2017-10-06 06:35:56', '2017-10-06 13:32:50', '12 Hours 131 Minutes', 12, 131, '2017-10-06'),
(2, 2, '2017-10-06 16:01:06', '', '', 0, 0, '2017-10-06'),
(3, 1, '2017-10-12 14:34:24', '2017-10-24 06:29:36', '30 Hours 109 Minutes', 30, 109, '2017-10-12'),
(4, 2, '2017-10-24 03:15:04', '2017-10-24 12:52:27', '9 Hours 37 Minutes', 9, 37, '2017-10-23'),
(5, 4, '2017-11-05 01:35:17', '2017-11-06 21:03:23', '19 Hours 28 Minutes', 19, 28, '2017-11-04');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(42, 'Portuguese', 1),
(41, 'Vietnamese', 2),
(40, 'French', 2),
(39, 'Spanish', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-id or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'Otp Sent to phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Send Succeffully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Driver has been arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Ride Has been Ended'),
(57, 29, 'en', 'Ride Book Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has been Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Cancel By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 1, 1, 4, '', 1, 4, '', '2017-10-06'),
(2, 4, 1, 4, '', 1, 4.5, '', '2017-10-06');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `booking_id`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 0, 2),
(3, 1, 1, 0, 3),
(4, 1, 1, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'Nikita .', 'nikita@gmail.com', '+917988782109', 'qwerty', '', '', 0, '0', 'Friday, Oct 6', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '4.25', 0, '', 1, '2017-10-06', 1),
(2, 1, 'DelivereUser .', 'user@gmail.com', '+919253026362', 'qwerty', '', '', 0, '0', 'Friday, Oct 6', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-06', 1),
(3, 1, 'Juan Sebastián .', 'jsgil@deliver-e.com', '+573137072180', 'zxcasdqwe123', '', '', 0, '0', 'Tuesday, Oct 24', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 1, 'E643B39BD3A3C754B49A314D03ECCA38B31517736C946F2E1660AAB9CF49A14B', 1, '3137E5D2-A031-4916-B9B3-29DA00EA6AE7', 0),
(2, 2, '3F0B0CF567454EABF7263509D7596061D9CCAAFBC422268504B3DF33075009DD', 1, 'F719F6A8-2A27-4306-A6F0-4CD77C63329F', 1),
(3, 2, '920AAE50AF2709099517CB8BFD678DC3B803D00B370A6EDB6FD4609D1835F29E', 1, '5347E382-D348-4244-B3E7-610D67B25B79', 1),
(4, 2, '0FB935B058984266978B0F66047C2F9EA8A1F00548C917B1B4475F5EB8D32FFC', 1, 'BDDC6EF4-CEF7-403C-ABE2-B7B65E8718B3', 1),
(5, 1, 'C84B7D7DA088D735BB4962BA95786FA0910DE24F679432E30329EDED0CF6DD61', 1, 'E6B8A35D-811A-4CF9-AA3A-C1C4AA8EC32A', 1),
(6, 2, '2C2B1E6ED2094B5EB9D4D10D077905EA6541C0AD0D650864F693A33C3F4D17C6', 1, '3BDDF5F9-9A65-4BF8-B69C-CC4B318D1916', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `content`, `image`, `link`) VALUES
(1, 'Cabs for Every Pocket', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket', 'webstatic/img/ola-article/why-ola-1.jpg', ''),
(2, 'Secure and Safer Rides', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', 'webstatic/img/ola-article/why-ola-3.jpg', ''),
(3, 'Apporio Select', 'A membership program with Ola that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time', 'webstatic/img/ola-article/why-ola-2.jpg', ''),
(4, '\r\nIn Cab Entertainment', 'Play music, watch videos and a lot more with Ola Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', 'webstatic/img/ola-article/why-ola-9.jpg', ''),
(5, 'Cashless Rides', 'Now go cashless and travel easy. Simply recharge your Apporio money or add your credit/debit card to enjoy hassle free payments.', 'webstatic/img/ola-article/why-ola-5.jpg', ''),
(6, 'Share and Express', 'To travel with the lowest fares choose Apporio Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away!', 'webstatic/img/ola-article/why-ola-3.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`configuration_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `configuration_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
